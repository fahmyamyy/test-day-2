name=$1
root="$name at $(date)"

mkdir "$root"
mkdir "$root"/about_me
mkdir "$root"/my_friends
mkdir "$root"/my_system_info

mkdir "$root"/about_me/personal
mkdir "$root"/about_me/professional

echo 'https://www.facebook.com/'$2 > "$root"/about_me/personal/facebook.txt
echo 'https://www.linkedin.com/in/'$3 > "$root"/about_me/professional/linkedin.txt

curl -v https://gist.githubusercontent.com/tegarimansyah/e91f335753ab2c7fb12815779677e914/raw/94864388379fecee450fde26e3e73bfb2bcda194/list%2520of%2520my%2520friends.txt -o "$root"/my_friends/list_of_my_friends.txt

echo 'My username:' $(id -un) > "$root"/my_system_info/about_this_laptop.txt && echo 'With host:' $(uname -a) >> "$root"/my_system_info/about_this_laptop.txt
echo 'Connection to google:' > "$root"/my_system_info/internet_connection.txt && ping forcesafesearch.google.com -t 3 >> "$root"/my_system_info/internet_connection.txt

